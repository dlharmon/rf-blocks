# OSHPark flex LRL calibration PCB

Data is for microstrip referenced to the bottom layer. Lines were 0.225 mm wide.

![photo of PCB](images/flex_photo.jpg)

## Connectors
[Rosenberger 08K80F-40ML5](https://products.rosenberger.com/_ocassets/db/08K80F-40ML5.pdf) They are 1.85 mm, extremely expensive, but quite repeatable.

## Stackup

## Insertion Loss
![plot of insertion loss](images/flex_il.png)

Zoom of lower frequencies:

![plot of insertion loss](images/flex_il_lf.png)

## Time domain
![image of time domain response](images/oshflex_td.png)