# OSHPark 6 layer LRL calibration PCB

This is not currently a standard OSHPark service, but a test panel.

Data is for microstrip referenced to the closest inner layer. Lines were 0.216 mm (8.5 mil) wide which should be 50 ohms masked and a bit higher unmasked.

![Photo of PCB](images/OSH6L_photo.jpg)

## Connectors
[Rosenberger 08K80F-40ML5](https://products.rosenberger.com/_ocassets/db/08K80F-40ML5.pdf) They are 1.85 mm, extremely expensive, but quite repeatable.

## Stackup
![image of stackup](images/6lstack.jpeg)
![image of controlled impedance data](images/6lz.png)

## Insertion Loss

![plot of insertion loss](images/OSH6L_il.png)

Zoom of lower frequencies:

![plot of insertion loss](images/OSH6L_il_lf.png)

## Time domain
 - number before extension: PCB number
 - 'm':masked, 'u': unmasked
 - 31: length in mm (excludes connector area)
 - ref3: These boards have no copper on Inner1, the first ground plane is inner 2. This is [https://gitlab.com/dlharmon/rf-blocks/-/tree/main/LRL_cal/OSHPark4L_cal](the 4 layer) with two blank layers inserted.

![image of time domain response](images/osh6l_td.png)
