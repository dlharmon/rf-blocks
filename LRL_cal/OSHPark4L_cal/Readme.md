# OSHPark 4 layer LRL calibration PCB

Data is for microstrip referenced to the closest inner layer. Lines were 0.381 mm (15 mil) wide which should be 50 ohms masked and a bit higher unmasked.

![photo of PCB](images/OSH4L_photo.jpg)

## Connectors
[Rosenberger 08K80F-40ML5](https://products.rosenberger.com/_ocassets/db/08K80F-40ML5.pdf) They are 1.85 mm, extremely expensive, but quite repeatable.

## Stackup
This was sent to me by Drew at OSHPark. It differs slightly from what is published on their site.
![image of stackup](images/4lstack.jpeg)

## Insertion Loss

Data is for microstrip referenced to the closest inner layer.

![plot of insertion loss](images/OSH4L_il.png)

Zoom of lower frequencies:

![plot of insertion loss](images/OSH4L_il_lf.png)

## Time domain
![image of time domain response](images/osh4l_td.png)