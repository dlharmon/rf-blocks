EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "180 degree splitter - MCL TC2-72T+"
Date "2020-02-02"
Rev "1"
Comp "Harmon Instruments"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L combined:ground #PWR?
U 1 1 5F1CCD2B
P 5700 4150
AR Path="/5BE20C19/5F1CCD2B" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5F1CCD2B" Ref="#PWR?"  Part="1" 
AR Path="/5F1CCD2B" Ref="#PWR0102"  Part="1" 
F 0 "#PWR0102" H 5700 4150 50  0001 C CNN
F 1 "ground" H 5700 4080 50  0001 C CNN
F 2 "" H 5700 4150 50  0001 C CNN
F 3 "" H 5700 4150 50  0000 C CNN
	1    5700 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:RF_Shield_One_Piece J1
U 1 1 5F23D8F6
P 7750 5150
F 0 "J1" H 8380 5139 50  0000 L CNN
F 1 "RF_Shield_One_Piece" H 8380 5048 50  0000 L CNN
F 2 "kicad_pcb:Laird_Technologies_BMI-S-209-F_29.36x18.50mm" H 7750 5050 50  0001 C CNN
F 3 "~" H 7750 5050 50  0001 C CNN
	1    7750 5150
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5F23E484
P 7750 5550
AR Path="/5BE20C19/5F23E484" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5F23E484" Ref="#PWR?"  Part="1" 
AR Path="/5F23E484" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 7750 5550 50  0001 C CNN
F 1 "ground" H 7750 5480 50  0001 C CNN
F 2 "" H 7750 5550 50  0001 C CNN
F 3 "" H 7750 5550 50  0000 C CNN
	1    7750 5550
	1    0    0    -1  
$EndComp
$Comp
L connector:9432D J2
U 1 1 5F23EF44
P 5700 4000
F 0 "J2" H 5800 4000 50  0000 L CNN
F 1 "9432D" H 5800 3909 50  0000 L CNN
F 2 "kicad_pcb:SMA_TH" H 5700 4150 50  0001 C CNN
F 3 "" H 5700 4000 50  0001 C CNN
	1    5700 4000
	1    0    0    -1  
$EndComp
$Comp
L connector:9432D J3
U 1 1 5F242DFC
P 4900 4000
F 0 "J3" H 4854 4238 50  0000 C CNN
F 1 "9432D" H 4854 4147 50  0000 C CNN
F 2 "kicad_pcb:SMA_TH" H 4900 4150 50  0001 C CNN
F 3 "" H 4900 4000 50  0001 C CNN
	1    4900 4000
	-1   0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5F243CF9
P 5700 4600
AR Path="/5BE20C19/5F243CF9" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5F243CF9" Ref="#PWR?"  Part="1" 
AR Path="/5F243CF9" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 5700 4600 50  0001 C CNN
F 1 "ground" H 5700 4530 50  0001 C CNN
F 2 "" H 5700 4600 50  0001 C CNN
F 3 "" H 5700 4600 50  0000 C CNN
	1    5700 4600
	1    0    0    -1  
$EndComp
$Comp
L connector:9432D J4
U 1 1 5F243D03
P 5700 4450
F 0 "J4" H 5800 4450 50  0000 L CNN
F 1 "9432D" H 5800 4359 50  0000 L CNN
F 2 "kicad_pcb:SMA_TH" H 5700 4600 50  0001 C CNN
F 3 "" H 5700 4450 50  0001 C CNN
	1    5700 4450
	1    0    0    -1  
$EndComp
$Comp
L misc:QBQTT U1
U 1 1 5E40967E
P 5150 3850
F 0 "U1" H 5300 4025 50  0000 C CNN
F 1 "QBQTT" H 5300 3934 50  0000 C CNN
F 2 "kicad_pcb:MCL_TTT166" H 5050 3900 50  0001 L CNN
F 3 "$PARTS/QBQTT/SYM-63LH+.pdf" V 5550 3700 50  0001 C CNN
	1    5150 3850
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5E40B685
P 4900 4150
AR Path="/5BE20C19/5E40B685" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5E40B685" Ref="#PWR?"  Part="1" 
AR Path="/5E40B685" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 4900 4150 50  0001 C CNN
F 1 "ground" H 4900 4080 50  0001 C CNN
F 2 "" H 4900 4150 50  0001 C CNN
F 3 "" H 4900 4150 50  0000 C CNN
	1    4900 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 4450 5300 4450
Wire Wire Line
	5300 4450 5300 4250
Text Label 5400 4450 0    50   ~ 0
R
Text Label 5550 4000 0    50   ~ 0
I
Text Label 5050 4000 0    50   ~ 0
L
$EndSCHEMATC
