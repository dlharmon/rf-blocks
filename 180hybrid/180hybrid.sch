EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "180 degree splitter - MCL TC2-72T+"
Date "2020-02-02"
Rev "1"
Comp "Harmon Instruments"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L combined:ground #PWR?
U 1 1 5EEF1AF3
P 4900 4450
AR Path="/5BE20C19/5EEF1AF3" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5EEF1AF3" Ref="#PWR?"  Part="1" 
AR Path="/5EEF1AF3" Ref="#PWR0109"  Part="1" 
F 0 "#PWR0109" H 4900 4450 50  0001 C CNN
F 1 "ground" H 4900 4380 50  0001 C CNN
F 2 "" H 4900 4450 50  0001 C CNN
F 3 "" H 4900 4450 50  0000 C CNN
	1    4900 4450
	-1   0    0    -1  
$EndComp
$Comp
L misc:transformer_CT T1
U 1 1 5F1B5C4A
P 5250 4200
F 0 "T1" H 5250 4581 50  0000 C CNN
F 1 "TC2-72T+" H 5250 4490 50  0000 C CNN
F 2 "kicad_pcb:Macom_SM-22" H 5250 4489 50  0001 C CNN
F 3 "" H 5250 4200 50  0001 C CNN
	1    5250 4200
	-1   0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5F1CCD2B
P 6600 4150
AR Path="/5BE20C19/5F1CCD2B" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5F1CCD2B" Ref="#PWR?"  Part="1" 
AR Path="/5F1CCD2B" Ref="#PWR0102"  Part="1" 
F 0 "#PWR0102" H 6600 4150 50  0001 C CNN
F 1 "ground" H 6600 4080 50  0001 C CNN
F 2 "" H 6600 4150 50  0001 C CNN
F 3 "" H 6600 4150 50  0000 C CNN
	1    6600 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 4000 6050 4000
Wire Wire Line
	6050 4400 5450 4400
Wire Wire Line
	4900 4150 4900 4400
Wire Wire Line
	5050 4400 4900 4400
Connection ~ 4900 4400
Wire Wire Line
	4900 4400 4900 4450
$Comp
L resistor:resistor_0402 R?
U 1 1 5F20D981
P 5700 4200
AR Path="/5C2CDB08/5F20D981" Ref="R?"  Part="1" 
AR Path="/5E374CB3/5F20D981" Ref="R?"  Part="1" 
AR Path="/5F20D981" Ref="R1"  Part="1" 
F 0 "R1" V 5600 4200 50  0000 C CNN
F 1 "24.9" V 5700 4200 50  0000 C CNN
F 2 "kicad_pcb:R_0805_2012Metric" V 5600 4000 50  0001 C CNN
F 3 "" H 5700 4200 50  0000 C CNN
	1    5700 4200
	0    1    1    0   
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5F225FE4
P 5950 4200
AR Path="/5BE20C19/5F225FE4" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5F225FE4" Ref="#PWR?"  Part="1" 
AR Path="/5F225FE4" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 5950 4200 50  0001 C CNN
F 1 "ground" H 5950 4130 50  0001 C CNN
F 2 "" H 5950 4200 50  0001 C CNN
F 3 "" H 5950 4200 50  0000 C CNN
	1    5950 4200
	0    -1   -1   0   
$EndComp
$Comp
L Device:RF_Shield_One_Piece J1
U 1 1 5F23D8F6
P 7750 5150
F 0 "J1" H 8380 5139 50  0000 L CNN
F 1 "RF_Shield_One_Piece" H 8380 5048 50  0000 L CNN
F 2 "RF_Shielding:Laird_Technologies_BMI-S-209-F_29.36x18.50mm" H 7750 5050 50  0001 C CNN
F 3 "~" H 7750 5050 50  0001 C CNN
	1    7750 5150
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5F23E484
P 7750 5550
AR Path="/5BE20C19/5F23E484" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5F23E484" Ref="#PWR?"  Part="1" 
AR Path="/5F23E484" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 7750 5550 50  0001 C CNN
F 1 "ground" H 7750 5480 50  0001 C CNN
F 2 "" H 7750 5550 50  0001 C CNN
F 3 "" H 7750 5550 50  0000 C CNN
	1    7750 5550
	1    0    0    -1  
$EndComp
$Comp
L connector:9432D J2
U 1 1 5F23EF44
P 6600 4000
F 0 "J2" H 6700 4000 50  0000 L CNN
F 1 "9432D" H 6700 3909 50  0000 L CNN
F 2 "kicad_pcb:SMA_TH" H 6600 4150 50  0001 C CNN
F 3 "" H 6600 4000 50  0001 C CNN
	1    6600 4000
	1    0    0    -1  
$EndComp
$Comp
L connector:9432D J3
U 1 1 5F242DFC
P 4900 4000
F 0 "J3" H 4854 4238 50  0000 C CNN
F 1 "9432D" H 4854 4147 50  0000 C CNN
F 2 "kicad_pcb:SMA_TH" H 4900 4150 50  0001 C CNN
F 3 "" H 4900 4000 50  0001 C CNN
	1    4900 4000
	-1   0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5F243CF9
P 6600 4550
AR Path="/5BE20C19/5F243CF9" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5F243CF9" Ref="#PWR?"  Part="1" 
AR Path="/5F243CF9" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 6600 4550 50  0001 C CNN
F 1 "ground" H 6600 4480 50  0001 C CNN
F 2 "" H 6600 4550 50  0001 C CNN
F 3 "" H 6600 4550 50  0000 C CNN
	1    6600 4550
	1    0    0    -1  
$EndComp
$Comp
L connector:9432D J4
U 1 1 5F243D03
P 6600 4400
F 0 "J4" H 6700 4400 50  0000 L CNN
F 1 "9432D" H 6700 4309 50  0000 L CNN
F 2 "kicad_pcb:SMA_TH" H 6600 4550 50  0001 C CNN
F 3 "" H 6600 4400 50  0001 C CNN
	1    6600 4400
	1    0    0    -1  
$EndComp
$Comp
L capacitors:capacitor_0603 C1
U 1 1 5F2523AB
P 6250 4000
F 0 "C1" V 5978 4000 50  0000 C CNN
F 1 "100 nF" V 6069 4000 50  0000 C CNN
F 2 "kicad_pcb:C_0603_1608Metric" H 6400 3900 50  0001 L CNN
F 3 "" H 6250 4000 50  0000 C CNN
	1    6250 4000
	0    1    1    0   
$EndComp
$Comp
L capacitors:capacitor_0603 C2
U 1 1 5F2548BB
P 6250 4400
F 0 "C2" V 5978 4400 50  0000 C CNN
F 1 "100 nF" V 6069 4400 50  0000 C CNN
F 2 "kicad_pcb:C_0603_1608Metric" H 6400 4300 50  0001 L CNN
F 3 "" H 6250 4400 50  0000 C CNN
	1    6250 4400
	0    1    1    0   
$EndComp
$EndSCHEMATC
