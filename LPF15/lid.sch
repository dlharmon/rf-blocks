EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L misc:testpoint_0.5mm TP1
U 1 1 5C5DBC35
P 4050 2850
F 0 "TP1" H 4200 2650 50  0000 L CNN
F 1 "testpoint_0.5mm" H 4250 2650 50  0001 L CNN
F 2 "kicad_pcb:TP_0.5mm" H 4250 2750 50  0001 L CNN
F 3 "" V 4300 2850 50  0001 C CNN
	1    4050 2850
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR0101
U 1 1 5C5DBF97
P 3950 3050
F 0 "#PWR0101" H 3950 3050 50  0001 C CNN
F 1 "ground" H 3950 2980 50  0001 C CNN
F 2 "" H 3950 3050 50  0001 C CNN
F 3 "" H 3950 3050 50  0000 C CNN
	1    3950 3050
	0    1    1    0   
$EndComp
$EndSCHEMATC
