#!/usr/bin/env python

from nosch import *

ldo_sot(80, 'CGK8Y', vi = '5V5', vo='5V')
c(80, c10u, '5V5', g, fp='0805')
c(81, c10u, '5V', g, fp='0805')
max1720(90, i='5V', o='N5V')

mcp1322(100, vdd=g, rst='EN', vss='N5V')
dmp3099(100, g='EN', d='vd1', s='5V')
r(100, r5k, 'EN', '5V')

sma_amp(1, 'i', fp='SMA_R')
maam011109(1, i='i', o='o', vc='vc', ve='N5V', vd='vd', bc='bc', vg='vg')
sma_amp(2, 'o', fp='SMA_R')

add('P3', 'DF3_2', 'DF3_2', [[1, '5V5'], [2, g]])

c(4, c100n, 'vg', g)

add('R8', 'POT_5K', 'POT_CT6', [[1, 'N2V'], [2, 'vc'], [3,'1V']])
r(9, r1k, 'N2V', 'N5V') # part pulls up
r(10, r5k, '1V', '5V')
c(9, c100n, 'vc', g)

nfm18ps(10, 'vd2', 'vd1')
l(10, 'blm18bb471', 'vd', 'vd2', fp='0603')

pcb.write("maam011109.net")
